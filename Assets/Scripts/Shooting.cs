﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Shooting : MonoBehaviour {

	public GameObject projectile;
	public float projectileSpeed;

	public Image ammoSlider;
	public float ammoCost;
	public float ammoRechargeRate;
    public float reloadCD;

	float _ammo = 100f;
    float _ammoCD = 0f;

    public Transform barrel;

	public KeyCode fire;

	GameObject _mainCam;
	PlayerMovement playerMov;
    public bool canShoot;

	void Start () {
		_mainCam = GameObject.FindGameObjectWithTag("MainCamera");
		playerMov = GetComponent<PlayerMovement>();
	}

	void Update () {
		if (Input.GetKeyDown(fire) && !playerMov.IsSprinting() && _ammo >= ammoCost && canShoot) {
			GameObject insProj = (GameObject)Instantiate(projectile, barrel.position, _mainCam.transform.rotation);
			insProj.GetComponent<Rigidbody>().velocity = _mainCam.transform.forward * projectileSpeed;
			_ammo -= ammoCost;
            _ammoCD = reloadCD;
            AudioMaster.Play("blow");
		}
        if (_ammoCD == 0)
            _ammo += ammoRechargeRate * Time.deltaTime;
        else
            _ammoCD = Mathf.Max(0, _ammoCD - Time.deltaTime);
		_ammo = Mathf.Clamp(_ammo, 0f, 100f);
		ammoSlider.fillAmount = _ammo/100f;
	}
}
