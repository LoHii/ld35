﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SpaceSnake : MonoBehaviour, StateObject {

    public GameObject holderPrefab;
    public float speed;
    public int balls;
    List<Vector3> _positions = new List<Vector3>();
    List<Transform> _holders = new List<Transform>();
    List<Vector3> _rotateSpeeds = new List<Vector3>();
    List<int> _indexes = new List<int>();
    Transform _target;

    public bool _active;

    public void Activate() {
        _active = true;
        print("Activate");
    }

    public void Deactivate() {
        _active = false;
        print("Deactivate");
    }

    void Awake() {
        _rotateSpeeds.Add(new Vector3(0, 0, Random.Range(45f, 135f) * (Random.Range(0, 2) * 2 - 1)));
        _holders.Add(transform.GetChild(0));
        for (int i = 1; i <= balls; i++) {
            _holders.Add(new GameObject().transform);
            _holders[i].name = "Snake Pieces";
            _holders[i].position = _holders[0].position;
            _holders[i].rotation = Quaternion.Euler(0, 0, Random.Range(0f, 360f));
            _holders[i].SetParent(_holders[0].parent, true);
            _rotateSpeeds.Add(new Vector3(0, 0, Random.Range(45f, 135f) * (Random.Range(0, 2) * 2 - 1)));
            Instantiate<GameObject>(holderPrefab).transform.SetParent(_holders[i], false);
        }
        _target = new GameObject().transform;
        _target.name = "Snake Target";
        NewTargetPos();
        _indexes.Add(0);
        _positions.Add(_holders[0].position);
    }

    void NewTargetPos() {
        if (_active) {
            _target.position = GameObject.Find("Player").transform.position + Vector3.up * 3;
        } else {
            _target.position = new Vector3(Random.Range(-24f, 24f), Random.Range(3f, 24f), Random.Range(-24f, 24f));
        }
    }

    void Update() {
        if (_active || _indexes.Count != balls) {

            _holders[0].GetChild(0).Rotate(_rotateSpeeds[0] * Time.deltaTime, Space.Self);
            _holders[0].rotation = Quaternion.RotateTowards(_holders[0].rotation, Quaternion.LookRotation(_target.position - _holders[0].position), 120f * Time.deltaTime);
            _holders[0].position += _holders[0].forward * speed * Time.deltaTime;

            _positions.Insert(0, _holders[0].position);

            if ((_target.position - _holders[0].position).magnitude < 0.1f)
                NewTargetPos();

            if (_indexes.Count == balls)
                _positions.RemoveAt(_positions.Count - 1);

            if ((_positions[_positions.Count - 1] - _positions[_indexes[_indexes.Count - 1]]).magnitude >= 5.0f && _indexes.Count != balls)
                _indexes.Add(_positions.Count - 1);

            for (int i = 1; i < _holders.Count; i++) {
                _holders[i].GetChild(0).Rotate(_rotateSpeeds[i] * Time.deltaTime, Space.Self);

                if (i < _indexes.Count) {
                    Vector3 target = (_positions[_indexes[i]] - _holders[i].position).normalized * Mathf.Min((_positions[_indexes[i]] - _holders[i].position).magnitude, speed * Time.deltaTime) + _holders[i].position;
                    _holders[i].LookAt(target, Vector3.up);
                    _holders[i].position = target;
                }
            }
        }
    }
}
