﻿using UnityEngine;
using System.Collections;

public class GunMovement : MonoBehaviour {

    public float MoveAmount = 1;

    public float MoveSpeed = 2;

    public GameObject Gun;

    public float MoveOnX;

    public float MoveOnY;

    public Vector3 defaultPos;

    public Vector3 NewGunPos;

    public bool ONOFF = false;

    PlayerMovement _pm;
    Shooting _s;
    
	void Start () {
        _pm = GameObject.Find("Player").GetComponent<PlayerMovement>();
        _s = GameObject.Find("Player").GetComponent<Shooting>();

        defaultPos = transform.localPosition;

        ONOFF = true;

	}
    
    void Update()
    {

        if (_pm.Sprint() == 0.25f || !_s.canShoot) {
            transform.parent.localRotation = Quaternion.Euler(Vector3.right * Mathf.Min(30.0f, transform.parent.localRotation.eulerAngles.x + 120.0f * Time.deltaTime));
        } else {
            transform.parent.localRotation = Quaternion.Euler(Vector3.right * Mathf.Max(0.0f, transform.parent.localRotation.eulerAngles.x - 120.0f * Time.deltaTime));
        }

            if (ONOFF == true)
        {

            MoveOnX = Input.GetAxis("Mouse X") * Time.deltaTime * MoveAmount;
            MoveOnY = Input.GetAxis("Mouse Y") * Time.deltaTime * MoveAmount;

            NewGunPos = new Vector3(defaultPos.x + MoveOnX, defaultPos.y + MoveOnY, defaultPos.z);

            Gun.transform.localPosition = Vector3.Lerp(Gun.transform.localPosition, NewGunPos, MoveSpeed * Time.deltaTime);

        }
        else {
            ONOFF = false;

            Gun.transform.localPosition = Vector3.Lerp(Gun.transform.localPosition, defaultPos, MoveSpeed * Time.deltaTime);
        }
    }
}
