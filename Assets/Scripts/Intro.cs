﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {

    public Transform ring;
    public GameObject ui;

    Shooting _s;
    Rigidbody _rb;
    public static bool introed;
    float _timer = -1;
    Rigidbody _player;
    float _rot = 5;
    bool first = true;
    AudioSource _audio;
    bool _there;

    void Start() {
        _rb = GameObject.Find("Player").GetComponent<Rigidbody>();
        _s = GameObject.Find("Player").GetComponent<Shooting>();

        StartCoroutine(StartRoutine());
    }

    IEnumerator StartRoutine() {
        yield return new WaitForSeconds(0.5f);
        if (introed)
            BeginBoss();
    }

    void OnTriggerEnter(Collider c) {
        if (!introed && c.tag == "Player") {
            introed = true;
            _audio = AudioMaster.Play("intro");
            _timer = (_audio.clip.length) / 2.0f - 1.5f;
            _rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePosition;
        }
    }

    void Update() {
        if (_timer > 0 && first) {
            _timer -= Time.deltaTime;
            _rot += Time.deltaTime * _rot;
            _rot = Mathf.Min(_rot, 405.0f);
            ring.Rotate(Vector3.up * _rot * Time.deltaTime);
            if (_timer < 0) {
                first = false;
                _timer = (_audio.clip.length) / 2.0f - 1.5f;
            }
        }
        if (_timer > 0 && !first) {
            _timer -= Time.deltaTime;
            _rot -= Time.deltaTime * _rot;
            _rot = Mathf.Max(_rot, 0);
            ring.Rotate(Vector3.up * _rot * Time.deltaTime);
            if (_rb.transform.position != Vector3.forward * -25f)
                _rb.transform.position = Vector3.MoveTowards(_rb.transform.position, Vector3.forward * -25f, 75f * Time.deltaTime);
            else if (!_there) {
                _there = true;
                CameraShaker.Shake(1.5f, 3f);
            }
            if (_timer < 0) {
                BeginBoss();
            }
        }
    }

    void BeginBoss() {
        BossStateManager.Fight();
        _rb.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ | RigidbodyConstraints.FreezePositionY;
        _s.canShoot = true;
        ui.SetActive(true);
        Destroy(this.gameObject);
    }
}
