﻿using UnityEngine;
using System.Collections;

public class ProjectileScript : MonoBehaviour {

	public float damage;
	Rigidbody _rb;

    Transform _attachedTo;
    Vector3 _attachDir;
    Vector3 _attachPos;
    Quaternion _attachRot;
    Quaternion _ownRot;

    void Start () {
		_rb = GetComponent<Rigidbody>();
        transform.Rotate(Vector3.forward, Random.Range(0f, 360f));
	}

	void OnTriggerEnter (Collider c) {
        if (c.tag == "Oculus") {
            Destroy(this.gameObject, 10.0f);
            GetComponent<CapsuleCollider>().enabled = false;
            return;
        }

            if (_attachedTo == null) {
            transform.position -= _rb.velocity*Time.deltaTime*0.75f;
            _rb.isKinematic = true;
            Attach(c.transform);
            Destroy(this.gameObject, 10.0f);
            GetComponent<CapsuleCollider>().enabled = false;
            if (c.tag == "BossBall" && c.name == "Weak Ball") {
                BossStateManager.TakeDamage(damage);
                print("Hit Boss");
            }
        }
	}

    void Attach(Transform t) {
        _attachedTo = t;
        _attachPos = t.position;
        _attachRot = t.rotation;
        _ownRot = Quaternion.Inverse(_attachRot) * transform.rotation;
        Vector3 _attachDifference = transform.position - t.position;
        _attachDir = Quaternion.Inverse(_attachRot) * _attachDifference;
    }
    void StayAttached() {
        if (_attachedTo.rotation != _attachRot) {
            _attachRot = _attachedTo.rotation;
        }
        
        if (_attachedTo.position != _attachPos)
            _attachPos = _attachedTo.position;

        transform.position = _attachPos + _attachRot * _attachDir;
        transform.rotation = _attachRot * _ownRot;
    }

    void LateUpdate() {
        if (_attachedTo != null) {
            StayAttached();
        }
    }
 }
