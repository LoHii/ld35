﻿using UnityEngine;
using System.Collections;

public interface StateObject {
    void Activate();
    void Deactivate();
}
