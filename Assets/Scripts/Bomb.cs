﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bomb : MonoBehaviour, StateObject {

    public AnimationCurve curve;
    List<Vector3> _startPos = new List<Vector3>();
    float _exploding = -1;

    void Start() {
        _startPos.Add(transform.position - Vector3.up*10.0f);

        for (int i = 0; i < transform.childCount; i++) {
            _startPos.Add(transform.GetChild(i).position);
            transform.GetChild(i).rotation = Quaternion.Euler(Random.Range(0, 360f), Random.Range(0, 360f), Random.Range(0, 360f));
        }
    }

    void Update() {
        if (_exploding > 0) {
            transform.position = _startPos[0] + new Vector3(RandShake(), RandShake(), RandShake()) + Vector3.up * 10.0f * curve.Evaluate(_exploding/3.0f);
            transform.Rotate(new Vector3(RandShake(), RandShake(), RandShake()));
            _exploding -= Time.deltaTime;

            if (_exploding <= 0) {
                for (int i = 0; i < transform.childCount; i++) {
                    Rigidbody rb = transform.GetChild(i).GetComponent<Rigidbody>();
                    rb.isKinematic = false;
                    SphereCollider sc = rb.GetComponent<SphereCollider>();
                    sc.enabled = true;
                    rb.AddExplosionForce(5.0f, new Vector3(_startPos[0].x, rb.position.y, _startPos[0].z), 7.0f, 0f, ForceMode.Impulse);
                }
            }
        }
    }

    public void Activate() {
        _exploding = 3.0f;
    }

    public void Deactivate() {
        _exploding = -1;
        transform.position = _startPos[0] + Vector3.up*10.0f;
        for (int i = 0; i < transform.childCount; i++) {
            Rigidbody rb = transform.GetChild(i).GetComponent<Rigidbody>();
            rb.isKinematic = true;
            rb.position = _startPos[i + 1];
            rb.transform.position = rb.position;
            SphereCollider sc = rb.GetComponent<SphereCollider>();
            sc.enabled = false;
        }
    }

    float RandShake() {
        return Random.Range(-(0.45f - 0.45f * curve.Evaluate(_exploding / 3.0f)), (0.45f - 0.45f * curve.Evaluate(_exploding / 3.0f)));
    }
}
