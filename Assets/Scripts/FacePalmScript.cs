﻿using UnityEngine;
using System.Collections;

public class FacePalmScript : MonoBehaviour, StateObject {

    public float timeToDrop;
    public float dropSpeed;
    public float moveSpeed;
    float _dropTimer;

    Transform player;
    float _startY;

    bool _active;

    public void Activate() {
        _dropTimer = 0;
        _active = true;
    }

    public void Deactivate() {
        _active = false;
    }

    void Start() {
        player = GameObject.Find("Player").transform;
        _startY = transform.position.y;
    }

    void Update() {
        if (_active) {
            if (transform.position.y == _startY || _dropTimer < 0)
                _dropTimer += Time.deltaTime;

            if (_dropTimer > timeToDrop) {
                Drop();
            } else {
                if (_dropTimer > 0)
                    FollowPlayer();
            }
        } else {
            Vector3 playPos = player.position.normalized * Mathf.Min(player.position.magnitude, 20.5f);
            transform.position = new Vector3(playPos.x, _startY, playPos.z);
            transform.LookAt(new Vector3(0, transform.position.y, 0));
        }
    }

    void Drop() {
        if (transform.position.y > 1f)
            transform.position += Vector3.down * dropSpeed * Time.deltaTime;
        else {
            _dropTimer = -2.0f;
            CameraShaker.Shake(1.0f, 1.0f);
        }
    }

    void FollowPlayer() {
        Vector3 playPos = transform.position;
        if (transform.position.y == _startY)
            playPos = player.position.normalized * Mathf.Min(player.position.magnitude, 20.5f);
        Vector3 followPos = new Vector3(playPos.x, _startY, playPos.z);
        if (_dropTimer < timeToDrop - 0.5f) {
            float speed;

            if (transform.position.y == _startY) {
                transform.Rotate(Vector3.up * 70f * Time.deltaTime);
                speed = moveSpeed;
            } else
                speed = moveSpeed * 0.4f;

            transform.position = Vector3.MoveTowards(transform.position, followPos, speed * Time.deltaTime);
        }
    }
}
