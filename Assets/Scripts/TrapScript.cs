﻿using UnityEngine;
using System.Collections;

public class TrapScript : MonoBehaviour, StateObject {

	public float moveSpeed;
	public float dirChangeInterval;
	public float spinSpeed;
	public float maxDistFromCenter;
	public float transitionSpeed;

	float _timer;
	Vector3 _dir;

	GameObject _player;

	bool _active;

	public void Activate () {
		_active = true;
	}

	public void Deactivate () {
		_active = false;
	}

	void Start () {
		_player = GameObject.Find("Player");
	}

	void Update () {
		if (_active) {
			if (transform.position.y > 0.2f) {
                Vector3 playPos = _player.transform.position.normalized * Mathf.Min(23, _player.transform.position.magnitude);
                transform.position = new Vector3(playPos.x, transform.position.y - moveSpeed*Time.deltaTime, playPos.z);
            }
			_timer += Time.deltaTime;
			while (_timer > dirChangeInterval) {
				_timer -= dirChangeInterval;

				float minX = -1f;
				float maxX = 1;

				float minZ = -1f;
				float maxZ = 1;

				if (transform.position.x > maxDistFromCenter)
					maxX = 0;
				if (transform.position.x < -maxDistFromCenter)
					minX = 0;
				if (transform.position.z > maxDistFromCenter)
					maxZ = 0;
				if (transform.position.z < -maxDistFromCenter)
					minZ = 0;

				float dirX = Random.Range(minX, maxX);
				float dirZ = Random.Range(minZ, maxZ);

				_dir = new Vector3(dirX, 0, dirZ).normalized;
			}
			transform.position += _dir * moveSpeed * Time.deltaTime;
			//transform.position = _player.transform.position;
			transform.rotation *= Quaternion.Euler(0, spinSpeed * Time.deltaTime, 0);
		} else {
			transform.position = _player.transform.position.normalized*Mathf.Min(23,_player.transform.position.magnitude) + new Vector3 (0,10,0);
		}
	}
}
