﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class WinLoseManager : MonoBehaviour {

    bool _stateSet;
    bool _death = true;

    public float timeToDeath;
    public float expStr;
    float _timer;

    public GameObject playerUI;
    public GameObject wonUI;
    public GameObject deathUI;

    public PlayerMovement player;
    public BossStateManager boss;

    float _playerHealth;
    float _bossHealth;

    GameObject[] _bossBalls;

    void Start() {
        _bossBalls = GameObject.FindGameObjectsWithTag("BossBall");
    }

    void Update() {

		if (Input.GetKeyDown(KeyCode.Escape)) {
			Application.Quit();
		}

        _playerHealth = player.health;
        _bossHealth = boss.health;
        if (!_stateSet) {
            if (_bossHealth < 0f) {
                _timer += Time.deltaTime;

                if (_death)
                    Death();

                if (_timer > timeToDeath && !_stateSet)
                    SetState(true);
            } else if (_playerHealth < 0f && !_stateSet) {
                SetState(false);
            }
        } else {
            if (Input.GetKeyDown(KeyCode.R)) {
                Reset();
            }
        }
    }

    void SetState(bool won) {
        if (won) {
            _stateSet = true;
            wonUI.SetActive(true);
            AudioMaster.Play("victory");
            int time = Mathf.CeilToInt(boss.WinTime());
            if (time < 60)
                wonUI.transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>().text += time + "s";
            else
                wonUI.transform.GetChild(0).GetChild(0).GetComponentInChildren<Text>().text += Mathf.FloorToInt(time / 60.0f) + "m " + (time - 60 * Mathf.FloorToInt(time / 60.0f)) + "s";

        } else {
            playerUI.SetActive(false);
            player.gameOver = true;
            boss.gameOver = true;
            _stateSet = true;
            deathUI.SetActive(true);
            GameObject.FindWithTag("MainCamera").SetActive(false);
        }
    }

    void Reset() {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    void Death() {
        player.GetComponent<Shooting>().canShoot = false;
        playerUI.SetActive(false);
        boss.gameOver = true;
        for (int i = 0; i < _bossBalls.Length; i++) {
            Rigidbody rb = _bossBalls[i].GetComponent<Rigidbody>();
            if (_bossBalls[i].transform.Find("Skull")) {
                Rigidbody srb = _bossBalls[i].transform.Find("Skull").GetComponent<Rigidbody>();
                srb.isKinematic = false;
                rb.useGravity = true;
                rb.AddExplosionForce(expStr, player.transform.position, Vector3.Distance(_bossBalls[i].transform.position, player.transform.position));
            }
            rb.isKinematic = false;
            rb.useGravity = true;
            rb.AddExplosionForce(expStr, player.transform.position, Vector3.Distance(_bossBalls[i].transform.position, player.transform.position));
        }
        _death = false;
    }
}
