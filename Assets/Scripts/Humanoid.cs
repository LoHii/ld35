﻿using UnityEngine;
using System.Collections;

public class Humanoid : MonoBehaviour {
    public Transform ikRight;


    Animator _animator;
    public bool punch;


    void Start() {
        _animator = GetComponent<Animator>();
    }

    // Update is called once per frame
    void OnAnimatorIK() {
        if (punch) {
            _animator.SetIKPositionWeight(AvatarIKGoal.RightHand, 1);
            _animator.SetIKRotationWeight(AvatarIKGoal.RightHand, 1);
            _animator.SetIKPosition(AvatarIKGoal.RightHand, ikRight.position);
            _animator.SetIKRotation(AvatarIKGoal.RightHand, ikRight.rotation);
        }
    }
}
