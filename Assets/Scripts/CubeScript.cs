﻿using UnityEngine;
using System.Collections;

public class CubeScript : MonoBehaviour, StateObject {

    public float speed;

    GameObject _player;

    bool _follow;

    void Start() {
        _player = GameObject.Find("Player");
    }

    void Update() {
        if (_follow)
            FollowPlayer();
        else
            transform.LookAt(_player.transform);
    }

    public void Activate() {
        _follow = true;
    }

    public void Deactivate() {
        transform.position = Vector3.up * 6f;
        _follow = false;
    }

    void FollowPlayer() {
        Vector3 playPos = _player.transform.position.normalized * Mathf.Min(_player.transform.position.magnitude, 23f);
        transform.position = Vector3.MoveTowards(transform.position, playPos, speed * Time.deltaTime);

        Quaternion q = Quaternion.LookRotation(_player.transform.position - transform.position);

        if (transform.position.magnitude >= 22.9f) {
            q = Quaternion.Euler(q.eulerAngles.x, q.eulerAngles.y + 90f, q.eulerAngles.z);
        }

        transform.rotation = Quaternion.RotateTowards(transform.rotation, q, 90f * Time.deltaTime);
    }
}
