﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AudioSlave))]
public class AudioSlaveEditor : Editor {
    AudioSlave slave;

    public override void OnInspectorGUI() {
        slave = (AudioSlave)target;
        DrawDefaultInspector();
        if (GUILayout.Button("Test sound")) {
            if (!slave.isPlaying() || !slave.isOver(3)) {
                slave.PlaySample();
            } else {
                slave.StopSample();
            }
        }
    }
}
