﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class AudioMaster : MonoBehaviour {

    static AudioMaster _am;
    public GameObject audioProxy;
    [HideInInspector]
    public Dictionary<string, AudioSlave> soundLibrary = new Dictionary<string, AudioSlave>();

    void Awake() {
        if (_am == null) {
            _am = this;
        } else {
            Debug.Log("Destroyed a duplicate AudioMaster");
            Destroy(gameObject);
            return;
        }

        DontDestroyOnLoad(gameObject);
    }

    public static AudioMaster GetMaster() {
        if (_am != null) {
            return _am;
        } else {
            print("No AudioMaster.");
            return null;
        }
    }

    public static AudioSource Play(string name) {
        return Play(name, Vector3.zero);
    }

    public static AudioSource Play(string name, Vector3 pos) {
        if (_am != null) {
            return _am.PlaySound(name, pos);
        } else {
            print("No AudioMaster.");
            return null;
        }
    }

    public static void Stop(AudioSource sound) {
        Stop(sound, 0);
    }

    public static void Stop(AudioSource sound, float fadeDuration) {
        if (_am != null) {
            _am.StopSound(sound, fadeDuration);
        } else {
            print("No AudioMaster.");
        }
    }

    AudioSource PlaySound(string name, Vector3 pos) {
        AudioSlave _as;
        if (soundLibrary.TryGetValue(name, out _as)) {

            GameObject _go = Instantiate(audioProxy);
            AudioSource _copy = _go.GetComponent<AudioSource>();
            AudioSource _og = _as.GetComponent<AudioSource>();

    
            BindingFlags _bf = BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance | 
                               BindingFlags.Default | BindingFlags.DeclaredOnly;
            PropertyInfo[] pinfos = (typeof(AudioSource)).GetProperties(_bf);
            foreach (var pinfo in pinfos) {
                if (pinfo.CanWrite) {
                    try {
                        if (!pinfo.IsDefined(typeof(System.ObsoleteAttribute), true))
                            pinfo.SetValue(_copy, pinfo.GetValue(_og, null), null);
                    } catch { }
                }
            }
            FieldInfo[] fields = (typeof(AudioSource)).GetFields();
            foreach (FieldInfo field in fields) {
                field.SetValue(_copy, field.GetValue(_og));
            }
            /*
            GameObject _go = Instantiate(_as.gameObject);
            Destroy(_go.GetComponent<AudioSlave>());
            _go.AddComponent(typeof(AudioProxy));
            AudioSource _copy = _go.GetComponent<AudioSource>();*/
            
            _copy.pitch = Random.Range(_as.pitchRange.x, _as.pitchRange.y);
            
            _copy.volume = Random.Range(_as.volRange.x, _as.volRange.y);

            _copy.transform.position = pos;
            if (pos == Vector3.zero)
                _copy.spatialBlend = 0;

            _copy.Play();
            return _copy;
        }

        Debug.LogError("No sound found with ID " + name + ".");
        return null;
    }
    
    void StopSound(AudioSource sound, float fade) {
        AudioProxy _ap = sound.GetComponent<AudioProxy>();
        if (_ap != null)
            _ap.Fade(fade);
        else
            Debug.LogError(sound.ToString() + " wasn't created with AudioMaster.");
    }
}
