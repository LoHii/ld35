﻿using UnityEngine;
using System.Collections;

public class AudioProxy : MonoBehaviour {
    AudioSource _as;
    float _deathTimer = 0;
    float _fade;
    float _fadeMax = -1;
    float _vol;

	void Start () {
        _as = GetComponent<AudioSource>();
        if (_as.clip != null)
            _deathTimer = _as.clip.length/_as.pitch;
	}
	
	void Update () {
        _deathTimer -= Time.deltaTime;

        if (_deathTimer <= 0) {
            if (_as.clip == null || !_as.loop) {
                print("No clip");
                Destroy(this.gameObject);
                return;
            } else
                _deathTimer = _as.clip.length;
        }

        if (_fadeMax > 0) {
            _fade -= Time.deltaTime;
            if (_fade < 0)
                Destroy(this.gameObject);
            else
                _as.volume = _vol * _fade / _fadeMax;
        }
	}

    public void Fade(float f) {
        if (f == 0)
            Destroy(gameObject);
        else {
            _fadeMax = f;
            _fade = f;
            _vol = _as.volume;
        }
    }
}
