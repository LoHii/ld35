﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(AudioSource))]
public class AudioSlave : MonoBehaviour {
    public string soundID;
    public Vector2 pitchRange = new Vector2(1, 1);
    public Vector2 volRange = new Vector2(1, 1);

    void Awake() {
        Transform p = transform.parent;
        while (p != null && p.name != "Audio Master") {
            p = p.parent;
        }

        AudioMaster _am = null;
        if (p != null)
            _am = p.GetComponent<AudioMaster>();

        if (_am != null && !_am.soundLibrary.ContainsKey(soundID))
           _am.soundLibrary.Add(soundID, this);
        else
            Debug.LogError("Multiple AudioSlaves with same soundID!");
    }

#if (UNITY_EDITOR)
    float ogPitch;
    float ogVol;
    AudioSource _as;

    public void PlaySample() {
        if (_as == null) {
            _as = GetComponent<AudioSource>();
            if (_as == null) {
                Debug.Log("No audioclip");
                return;
            }
        }

        _as.pitch = Random.Range(pitchRange.x, pitchRange.y);
        _as.volume = Random.Range(volRange.x, volRange.y);
        _as.Play();
    }

    public void StopSample() {
        if (_as == null) {
            _as = GetComponent<AudioSource>();
            if (_as == null) {
                Debug.Log("No audioclip");
                return;
            }
        }

        _as.Stop();
    }

    public bool isPlaying() {
        if (_as == null) {
            _as = GetComponent<AudioSource>();
            if (_as == null) {
                Debug.Log("No audioclip");
                return false;
            }
        }

        return _as.isPlaying;
    }

    public bool isOver(float amount) {
        if (_as == null) {
            _as = GetComponent<AudioSource>();
            if (_as == null) {
                Debug.Log("No audioclip");
                return false;
            }
        }

        return (_as.clip.length >= amount);
    }
#endif
}
