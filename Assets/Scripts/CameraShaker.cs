﻿using UnityEngine;
using System.Collections;

public class CameraShaker : MonoBehaviour {

    static CameraShaker _cam;
    float _shaking;
    float _dur;
    float _maxDur;

	void Awake() {
        _cam = this;
    }

	void Update () {
        if (_dur > 0) {
            float shake = _shaking * _dur / _maxDur;
            transform.localPosition = new Vector3(Random.Range(-shake, shake), Random.Range(-shake, shake) + 3f, Random.Range(-shake, shake));
            _dur -= Time.deltaTime;
        } else
            transform.localPosition = Vector3.up * 3;
    }

    public static void Shake(float shake, float duration) {
        _cam._shaking = shake;
        _cam._dur = duration;
        _cam._maxDur = duration;
    }
}
