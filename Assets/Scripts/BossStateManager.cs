﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class BossStateManager : MonoBehaviour {

    static BossStateManager _bsm;

    public bool useHealthTimeState;
    public float health;
    public float damageToStateChange;
    public float timeToStateChange;
    float _damageTakenThisState;
    float _stateTimer;

    int _currentIndex = -1;
    List<StateStruct> _stateList = new List<StateStruct>();

    public Transform player;
    public float changeDuration;

    public Transform hand;
    public Transform guy;
    public Transform trap;
    public Transform snake;
    public Transform cube;
    public Transform bomb;
    public Image hpBar;

    public AnimationCurve curve;

    StateStruct _current;

    List<Transform> _bossBalls = new List<Transform>();

    StateStruct _handStruct;
    StateStruct _trapStruct;
    StateStruct _guyStruct;
    StateStruct _snakeStruct;
    StateStruct _cubeStruct;
    StateStruct _bombStruct;

    List<Vector3> _startPos = new List<Vector3>();
    List<Quaternion> _startRot = new List<Quaternion>();

    Transform _weakBall;
    Transform _skull;

    float _progress;
    AudioSource _loop;
    bool _eyes = true;
    float _time = -1;

	[HideInInspector]
	public bool gameOver;

    struct StateStruct {
        public Transform root;
        public List<Transform> nodes;
        public StateObject so;

        public StateStruct(Transform t) {
            this.nodes = FindNodes(t);
            this.so = t.GetComponent<StateObject>();
            this.root = t;
            _bsm._stateList.Add(this);
        }
    }


    void Start() {
        _bsm = this;
        GameObject[] goArray = GameObject.FindGameObjectsWithTag("BossBall");

        for (int i = 0; i < goArray.Length; i++) {
            _bossBalls.Add(goArray[i].transform);
            if (goArray[i].name == "Weak Ball") {
                _weakBall = goArray[i].transform;
                _skull = _weakBall.GetChild(0);
                _skull.SetParent(transform, true);
            }
        }

        StopEye();

        _cubeStruct = new StateStruct(cube);
        _bombStruct = new StateStruct(bomb);
        //_guyStruct = new StateStruct(guy);
        _trapStruct = new StateStruct(trap);
        _handStruct = new StateStruct(hand);
        //_snakeStruct = new StateStruct(snake);
    }

    void LateUpdate() {

		if (!gameOver) {

			if (_current.nodes != null)
				MoveTo(_current);

			if (useHealthTimeState) {
				_stateTimer += Time.deltaTime;
				if (_stateTimer > timeToStateChange) {
					NextState();
				}
			}

            if (_time >= 0)
                _time += Time.deltaTime;

			hpBar.fillAmount = health / 100f;
		} else if (_loop != null) {
            AudioMaster.Stop(_loop, 0f);
            GameObject boom = _weakBall.GetChild(0).gameObject;
            boom.transform.SetParent(null, true);
            boom.SetActive(true);
            Destroy(_skull.gameObject);
            CameraShaker.Shake(2.0f, 2.0f);
            StopEye();
            _loop = null;
        }
    }

    void MoveTo(StateStruct ss) {

        List<Transform> nodes = ss.nodes;

        if (_progress != 1) {
            if (changeDuration != 0)
                _progress = Mathf.Min(1, _progress + Time.deltaTime / changeDuration);
            else
                _progress = 1;

            if (_progress >= 0.9f && !_eyes && _loop != null)
                StartEye();

            if (_progress == 1) {
                if (_loop == null) {
                    _loop = AudioMaster.Play("loop");
                    StartEye();
                    _time = 0;
                }
                _current.so.Activate();
            }
        }

        for (int i = 0; i < _bossBalls.Count; i++) {
            if (i < nodes.Count) {
                _bossBalls[i].position = Vector3.Lerp(_startPos[i], nodes[i].position, curve.Evaluate(_progress));
                _bossBalls[i].rotation = Quaternion.Lerp(_startRot[i], nodes[i].rotation, curve.Evaluate(_progress));
            }
        }

        _skull.position = _weakBall.position;
        Quaternion target = Quaternion.LookRotation((player.GetChild(1).position - _skull.position));
        _skull.rotation = Quaternion.RotateTowards(_skull.rotation, target, 180f * Time.deltaTime);
    }

    public static List<Transform> FindNodes(Transform root) {
        List<Transform> list = new List<Transform>();

        for (int i = 0; i < root.childCount; i++) {
            Transform child = root.GetChild(i);

            if (child.tag == "BossNode")
                list.Add(child);
            else {
                List<Transform> childList = FindNodes(child);
                list.AddRange(childList);
            }
        }

        return list;
    }

    void ChangeCurrent(StateStruct ss) {
        StopEye();
        _stateTimer = 0;
        _damageTakenThisState = 0;
        if (_current.so != null)
            _current.so.Deactivate();
        _current = ss;
        _progress = 0;
        Shuffle(_bossBalls);
        _startPos.Clear();
        _startRot.Clear();
        for (int i = 0; i < _bossBalls.Count; i++) {
            _startPos.Add(_bossBalls[i].position);
            _startRot.Add(_bossBalls[i].rotation);
        }
    }

    void Shuffle(List<Transform> list) {
        for (int i = 0; i < list.Count; i++) {
            Transform temp = list[i];
            int randomIndex = Random.Range(i, list.Count);
            list[i] = list[randomIndex];
            list[randomIndex] = temp;
        }
    }

    public static void TakeDamage(float damage) {
        if (_bsm != null)
            _bsm.GetDamaged(damage);
    }

    void GetDamaged(float damage) {
        if (_progress != 1)
            return;

        AudioMaster.Play("hiss");
        health -= damage;
        _damageTakenThisState += damage;
        if (_damageTakenThisState >= damageToStateChange) {
            NextState();
        }
    }

    void NextState() {
        _currentIndex++;
        if (_currentIndex == _stateList.Count)
            _currentIndex = 0;

        ChangeCurrent(_stateList[_currentIndex]);
    }

    public static float Damage() {
        return _bsm.StateDamage();
    }

    float StateDamage() {
        if (_progress != 1 || gameOver)
            return 0;

        float dmg = 10;

        if (_current.root == cube) {
            dmg = 25f;
            NextState();
        }

        return dmg;
    }

    public static void Fight() {
        if (_bsm != null) {
            if (_bsm._currentIndex == -1) {
                _bsm.useHealthTimeState = true;
                _bsm.NextState();
            }
        }
    }

    void StartEye() {
        if (_eyes)
            return;

        for (int i = 0; i < _bossBalls.Count; i++) {
            if (_bossBalls[i].name != "Weak Ball")
                for (int j = 0; j < _bossBalls[i].childCount; j++) {
                    _bossBalls[i].GetChild(j).gameObject.SetActive(true);
            }
        }
        _eyes = true;
    }

    void StopEye() {
        if (!_eyes)
            return;

        for (int i = 0; i < _bossBalls.Count; i++) {
            if (_bossBalls[i].name != "Weak Ball")
                for (int j = 0; j < _bossBalls[i].childCount; j++) {
                _bossBalls[i].GetChild(j).gameObject.SetActive(false);
            }
        }
        _eyes = false;
    }

    public float WinTime() {
        return _time;
    }
}
