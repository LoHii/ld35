﻿using UnityEngine;
using System.Collections;

public class UIShow : MonoBehaviour {

    float _amount = 200;
    float _duration = 1;
    float _current;

    void Start() {
        _current = _duration;
        _amount *= Screen.height / 450f;
    }

    void Update() {
        if (_current > 0) {
            for(int i = 0; i < transform.childCount; i++) {
                transform.GetChild(i).transform.position += Vector3.up * _amount * Time.deltaTime / _duration;
            }
            _current -= Time.deltaTime;
        }  else
            Destroy(this);
    }
}
